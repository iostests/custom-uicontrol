//
//  RangeSlider.swift
//  Zadatak5
//
//  Created by Jelena Mehic on 11/5/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

import UIKit

@IBDesignable
class RangeSlider: UIControl {
    
    
    @IBInspectable var lineColor: UIColor = UIColor.greenColor()
    @IBInspectable var borderColor: UIColor = UIColor(red: (238.0/255.0), green: (32.0/255), blue: (53.0/255.0), alpha: 1.0)
    @IBInspectable var thumbColor: UIColor = UIColor.greenColor()
    @IBInspectable var borderWidth: CGFloat = 1
    var selectedMin: Int = 20 {
        didSet{
            sendActionsForControlEvents(UIControlEvents.ValueChanged)
        }
    }
    var selectedMax: Int = 90 {
        didSet{
            sendActionsForControlEvents(UIControlEvents.ValueChanged)
        }
    }

    @IBInspectable var minValue: Int = 0 //ne pokazuje u ib
    @IBInspectable var maxValue: Int = 100
    
    
    let diameter:double_t  = 35
    var firstTime = true
    
    var leftThumbViewSelected = false
    var rightThumbViewSelected = false
    
    var thumbViewLeft: UIView = {
        let t = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        return t
    }()
    
    var thumbViewRight: UIView = {
        let t = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        return t
    }()
    
    var line = UIView()
    
    
    //--------2 init-a-------
    //obican i storyboard init
    override init(frame: CGRect) {
        super.init(frame: frame)
        initCustom()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initCustom()
    }
    
    override func prepareForInterfaceBuilder() {
        initCustom()
    }


    func initCustom() {
        
        addSubview(line)
        addSubview(thumbViewLeft)
        addSubview(thumbViewRight)
        
        line.layer.borderColor = borderColor.CGColor
        line.layer.borderWidth = borderWidth
        line.backgroundColor = lineColor
        
        thumbViewLeft.backgroundColor = thumbColor
        thumbViewRight.backgroundColor = thumbColor
        
        
        
        if firstTime {
            firstTime = false
            //----line----
            line.frame = CGRect(x: 0, y: frame.size.height/2, width: frame.size.width, height: 2)
            line.userInteractionEnabled = false
            
            //----left thumb----
            thumbViewLeft.userInteractionEnabled = false
            thumbViewLeft.layer.cornerRadius = thumbViewLeft.frame.size.width/2
            thumbViewLeft.layer.masksToBounds = true
            
            //----right thumb----
            thumbViewRight.userInteractionEnabled = false
            thumbViewRight.layer.cornerRadius = thumbViewRight.frame.size.width/2
            thumbViewRight.layer.masksToBounds = true
            
            
            let k1 = CGFloat(selectedMin)
            thumbViewLeft.layer.position = CGPoint(x: (frame.size.width - CGFloat(diameter))/CGFloat(maxValue-minValue) * k1 + CGFloat(diameter/2), y: frame.size.height/2)
            
            let k2 = CGFloat(selectedMax)
            thumbViewRight.layer.position = CGPoint(x: (frame.size.width - CGFloat(diameter))/CGFloat(maxValue-minValue) * k2 + CGFloat(diameter/2), y: frame.size.height/2)
        
            
        }
        
        
        if leftThumbViewSelected {
                
                let k1 = CGFloat(selectedMin)
                thumbViewLeft.layer.position = CGPoint(x: (frame.size.width - CGFloat(diameter))/CGFloat(maxValue-minValue) * k1 + CGFloat(diameter/2), y: frame.size.height/2)
                
            } else if rightThumbViewSelected {
                
                let k2 = CGFloat(selectedMax)
                thumbViewRight.layer.position = CGPoint(x: (frame.size.width - CGFloat(diameter))/CGFloat(maxValue-minValue) * k2 + CGFloat(diameter/2), y: frame.size.height/2)
        
            }
        
        
        

        setNeedsDisplay()
    }
    
    //--------layoutSubviews--------
    override func layoutSubviews() {
        super.layoutSubviews()
        initCustom()
    }
    
    //???
//    override func setNeedsLayout() {
//        super.setNeedsLayout()
//        initCustom()
//    }
}

    
    //MARK: - UIControl 
extension RangeSlider {
    //---------tracking began-------
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        super.beginTrackingWithTouch(touch, withEvent: event)
    
        let touchLocation = touch.locationInView(self)
    
        if CGRectContainsPoint(thumbViewRight.frame, touchLocation) && CGRectContainsPoint(thumbViewLeft.frame, touchLocation) {
           
            if touchLocation.x > frame.size.width/2 {
                leftThumbViewSelected = true
            } else {
                rightThumbViewSelected = true
            }
            
            return true
            
        } else
        
        //----desni thumbView
        if CGRectContainsPoint(thumbViewRight.frame, touchLocation) {
            
            rightThumbViewSelected = true
            
            UIView.animateWithDuration(0.2, delay: 0,
                 options: .CurveEaseOut,
                 animations: { [weak self]() -> Void in
                    self?.thumbViewRight.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1)
                },
                 completion: nil)
            
            return true
            
        //----levi thumbView
        } else if CGRectContainsPoint(thumbViewLeft.frame, touchLocation) {
            
            leftThumbViewSelected = true
            
            UIView.animateWithDuration(0.3, delay: 0,
                options: .CurveEaseInOut,
                animations: { [weak self]() -> Void in
                self?.thumbViewLeft.layer.transform = CATransform3DMakeScale(1.6, 1.6, 1)
                },
                completion: nil)
            
            return true
        }
        return false
    }
    
    //---------contrinue tracking----
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        super.continueTrackingWithTouch(touch, withEvent: event)
    
        //-----touch location-------
        let touchLocation = touch.locationInView(self)
        
        let m = (touchLocation.x - CGFloat(diameter/2)) * CGFloat(maxValue - minValue)
        
        let selectedValue = Int(CGFloat(minValue) + m / (frame.size.width - CGFloat(diameter)))
        
        //-----desni thumb
        if rightThumbViewSelected  {
            
            if selectedValue > maxValue {
                selectedMax = maxValue
            } else if selectedValue < selectedMin {
                selectedMax = selectedMin
            } else {
                selectedMax = selectedValue
            }

        //-----levi thumbView
        } else if leftThumbViewSelected {
            
            if selectedValue < minValue {
                selectedMin = minValue
            } else if selectedValue > selectedMax {
                selectedMin = selectedMax
            } else {
                selectedMin = selectedValue
            }
        
        }
        
    setNeedsLayout()
        
        
    return true
        
    } //nije implementirana metoda koja pretvara minimum vrednost u x promenljivu
    
    //---------end tracking--------
    override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
        
        super.endTrackingWithTouch(touch, withEvent: event)
        
        //let touchLocation = touch!.locationInView(self)
        //-----desni thumbView
        if rightThumbViewSelected  {
            
           UIView.animateWithDuration(0.2, delay: 0,
                options: .CurveEaseInOut,
                animations: { [weak self] () -> Void in
                    if let weakSelf = self {

                        weakSelf.thumbViewRight.layer.transform = CATransform3DIdentity
                    }
                },
                completion: nil)
            
        //-----levi thumbView
        } else if leftThumbViewSelected {
            
            UIView.animateWithDuration(0.2, delay: 0,
                options: .CurveEaseInOut,
                animations: { [weak self] () -> Void in
                    if let weakSelf = self {
                        
                        weakSelf.thumbViewLeft.layer.transform = CATransform3DIdentity
                    }
                },
                completion: nil)
        }
        
        leftThumbViewSelected = false
        rightThumbViewSelected = false

    }
}



