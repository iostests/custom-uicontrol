//
//  ViewController.swift
//  Zadatak5
//
//  Created by Jelena Mehic on 11/5/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var myRangeSlider: RangeSlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myRangeSlider.hidden = false
        
    }

    
}

